<!DOCTYPE html>
<html lang="en">
<head>
    <!-- 
        Nama    = Ida Bagus Ari Mahendra Putra
        Email   = ibarimahendrap@gmail.com
        No      = 081339001359 
    -->

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test Phase 2 - Island Media Management</title>

    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <?php
        $menu[0] = array('name' => 'coffe','img' => 'gallery_1.jpg');
        $menu[1] = array('name' => 'juice','img' => 'gallery_2.jpg');
        $menu[2] = array('name' => 'breakfast','img' => 'gallery_3.jpg');
        $menu[3] = array('name' => 'sandwich','img' => 'gallery_4.jpg');
    ?>

</head>
<body>
    <header>
        <nav class="nav">
            <ul class="nav__items">
                <li class="nav__logo"><a href="#"><img src="assets/icon/logo.svg"></li>
                <li class="nav__link"><a href="#about">about</a></li>
                <li class="nav__link"><a href="#menu">menu</a></li>
                <li class="nav__link"><a href="#moods">moods</a></li>
                <li class="nav__link"><a href="#blog">blog</a></li>
                <li class="nav__link"><a href="#contact">contact</a></li>
                <li class="nav__link"><a href="#search"><span class="fa fa-search"></span></a></li>
                
                <li class="nav__toggle"><span class="nav__toggle_bars"></span></li>
            </ul>
        </nav>
    </header>
    <main>
        <section id="banner">
            <div class="banner">
                <div class="banner__body">
                    <div class="banner__title">
                        <h1>Life begins after Coffee.</h1>
                    </div>
                    <div class="banner__button">
                        <a href="#menu" class="btn btn-style">view menu</a>
                    </div>
                </div>
            </div>
        </section>

        <section id="about">
            <div class="about">
                <div class="about__title">
                    <h1>What would you like to have?</h1>
                    <p>Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder.</p>
                </div>
                <div class="about__list">
                    <div class="row"> 
                        <?php
                            foreach ($menu as $i => $rs) :
                                $url = "assets/image/$rs[img]";
                                ?>
                                    <div class="about__column">
                                        <div class="container">
                                            <div class="overlay"></div>
                                            <img src="<?=$url?>" alt="" class="about__column_image">
                                            <h2><?=$rs['name']?></h2>
                                        </div>
                                    </div>
                                    <?php
                            endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </section>

        <section id="menu">
            <div class="menu">
                <div class="menu__title">
                    <h1>Extraction instant that variety white robusta strong</h1>
                    <p>Coffee plunger pot sweet barista, grounds acerbic coffee instant crema cream in half and half. Spoon lungo variety, as, siphon, ristretto, iced brewed and acerbic affogato grinder. Mazagran café au lait wings spoon, percolator milk latte dark strong. Whipped, filter latte, filter aromatic grounds doppio caramelization half and half.</p>
                </div>
                <div class="menu__button">
                    <a href="#contact" class="btn btn-style">contact us</a>
                </div>
            </div>
        </section>

        <section id="moods">
            <div class="moods">
                <div class="moods__title">
                    <h1>Health Benefits of Coffee</h1>
                </div>
                <div class="moods__list">
                    <div class="moods__item">
                        <img src="assets/icon/battery-full.svg">
                        <p>BOOST ENERGY LEVEL</p>
                    </div>
                    <div class="moods__item">
                        <img src="assets/icon/sun.svg">
                        <p>BOOST ENERGY LEVEL</p>
                    </div>
                    <div class="moods__item">
                        <img src="assets/icon/weight.svg">
                        <p>BOOST ENERGY LEVEL</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="blog">
            <div class="blog">
                <div class="blog__image">
                    <img src="assets/image/blog_1.jpg" >
                </div>
                <div class="blog__text">
                    <h2>blog</h2>
                    <h1>Qui espresso, grounds to go</h1>
                    <p>December 12, 2019 | Espresso</p>
                    <h3>Skinny caffeine aged variety filter saucer redeye, sugar sit steamed eu extraction organic. Beans, crema half and half fair trade carajillo in a variety dripper doppio pumpkin spice cup lungo, doppio, est trifecta breve and, rich, extraction robusta a eu instant. Body sugar steamed, aftertaste, decaffeinated coffee fair trade sit, white shop fair trade galão, dark crema breve frappuccino iced strong siphon trifecta in a at viennese.</h3>
                    <a href="#read">read more&nbsp;&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
        </section>

        <section id="contact">
            <div class="contact">
                <div class="contact__image">
                    <img src="assets/icon/logo.svg" >
                </div>
                <div class="contact__text">
                    <p>2800 S White Mountain Rd | Show Low AZ 85901<br>(928) 537-1425 | info@grinder-coffee.com</p>
                    <div style="margin-top: 10px;">
                        <span class="fa fa-instagram"></span>&nbsp;&nbsp;
                        <span class="fa fa-facebook-square"></span>
                    </div>
                </div>
                <div class="contact__news">
                    <p>newsletter</p>
                    <div class="contact__news_form">
                        <input type="text" placeholder="YOUR EMAIL ADDRESS">
                        <a href="#news" class="btn btn-style">subscribe</a>
                    </div>
                </div>
            </div>
        </section>
    </main>

    <script src="assets/js/script.js"></script>
</body>
</html>