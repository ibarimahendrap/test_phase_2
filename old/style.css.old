@font-face {font-family: "Crimson";src: url('../fonts/crimson_text/CrimsonText-Regular.ttf');}
@font-face {font-family: "Barlow Condensed";src: url('../fonts/barlow_condensed/BarlowCondensed-Regular.ttf');}
@font-face {font-family: "Barlow Condensed L";src: url('../fonts/barlow_condensed/BarlowCondensed-Light.ttf');}
@font-face {font-family: "Barlow Condensed T";src: url('../fonts/barlow_condensed/BarlowCondensed-Thin.ttf');}

* {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
}
html {
	font-family: 'Barlow Condensed T';
	scroll-behavior: smooth;
}

header {
    position: fixed;
    z-index: 1000;
    left: 0;
    top: 0;
    width: 100%;
    height: auto;
}

nav {
    background: transparent;
    padding: 0 10em;
}

ul {
    list-style-type: none;
}

img {
    height: 100%;
    width: 100%;
    /* object-fit: cover; */
}

a {
    color: white;
    text-decoration: none;
}

button{
    font-family: 'Barlow Condensed L';
    letter-spacing: 1px;
    padding: 7px 35px;
}

section h1 {
    font-size: 2.7em;
}

article {
    width: 100%;
}


.nav-items li:not(.logo) {
    /* font-size: 1em; */
    letter-spacing: 3px;
    font-size: 1.2em;
    padding: 10px 30px;
    white-space: nowrap;
    font-weight: lighter;
    text-transform: uppercase;
}
.nav-items li.logo img{
    width: 4em;
    padding: 10px 0;
}

.toggle-bars {
    order: 1;
}

.toggle-bars span {
    vertical-align: super;
}


.nav-items {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    align-items: center;
}


.nav-item {
    width: 100%;
    text-align: center;
    order: 2;
    display: none;
}
.nav-item.active {
    display: block;
}

.toggle-bars {
    cursor:pointer;
}
.bars {
    background: #999;
    display: inline-block;
    height: 2px;
    position: relative;
    transition: background .2s ease-out;
    width: 18px;
}
.bars:before,
.bars:after {
    background: #999;
    content: '';
    display: block;
    height: 100%;
    position: absolute;
    transition: all .2s ease-out;
    width: 100%;
}
.bars:before {
    top: 5px;
}
.bars:after {
    top: -5px;
}


.btn.btn-style {
    background-color: rgba(242, 196, 109, 1);
    color: white;
    text-transform: uppercase;
    border: 0;
    cursor: pointer;
}

.row {
    padding: 0 4px;
    display: flex;
    display: -ms-flexbox;
    flex-wrap: wrap;
    -ms-flex-wrap: wrap;
}
  
.column {
    margin-top: 8px;
    padding: 0 4px;
    flex: 25%;
    max-width: 25%;
    -ms-flex: 25%;

}

  
.column img {
    width: 100%;
    vertical-align: middle;
}


#banner {
	background-image: url(../image/header.jpg);
	position: relative;
	height: 100%;
    background-position: right;
    background-repeat: no-repeat;
    background-size: cover;
	z-index: 1;
}

#banner .banner-body {
    margin: 0 auto;
	padding: 0 50px;
    max-width: 1500px;
    min-height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
}

#banner .banner-body div{
    text-align: center;
}

#banner .banner-body .banner-title{
    position: absolute;
    top: 150px;
    left: 0;
    width: 100%;
}

#banner .banner-body .banner-title h1{
    color: white;
    font-size: 5em;
    font-family: 'Crimson';
    font-weight: lighter;
    letter-spacing: 3px;
}

#banner .banner-body .banner-button{
    position: absolute;
    bottom: 135px;
    left: 0;
    width: 100%;
}

#banner .banner-button a, #menu .menu-button a{
    font-family: 'Barlow Condensed L';
    letter-spacing: 1px;
    padding: 7px 35px;
}





#about, #menu{
    font-family: 'Crimson';
    padding: 50px;
}

#about {
    padding-top: 80px;
}

/* #about .about-body{
} */

#about .about-body .about-title, #menu .menu-body .menu-title, #moods .moods-body .moods-title{
    text-align: center;
    margin: auto;
    max-width: 730px;
    
}

#about .about-body .about-title h1, #menu .menu-body .menu-title h1, #moods .moods-body .moods-title h1{
    margin-bottom: 10px;
}

#about .about-body .about-title p, #menu .menu-body .menu-title p{
    margin-bottom: 25px;
}

#about .about-list .img{
    height: 100%;
    background-position: right;
    background-repeat: no-repeat;
    background-size: cover;
    min-height: 500px;
    display: flex;
    justify-content: center;
    align-items: center;
}

#about .about-list .container {
    position: relative;
    width: 100%;
}

#about .about-list .image {
    display: block;
    width: 100%;
    height: 100%;
    max-height: 500px;
}

#about .about-list .overlay {
    position: absolute;
    bottom: 0;
    left: 0;
    right: 0;
    background-color:rgba(255,255,255,.3);
    overflow: hidden;
    width: 0;
    height: 100%;
    transition: .5s ease;
}

#about .about-list .column:hover .overlay {
    width: 100%;
}

#about .about-list .text {
    color: white;
    font-size: 20px;
    position: absolute;
    top: 50%;
    left: 50%;
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
    transform: translate(-50%, -50%);
    white-space: nowrap;
}

#about .about-list .container p{
    color: white;
    font-family: 'Barlow Condensed';
    font-size: 2em;
    text-transform: uppercase;
    height: 100%;
    width: 100%;
    text-align: center;
    position: absolute;
    top: 45%;
}


#menu {
    padding-top: 80px;
}

#menu .menu-button{
    padding-top: 20px;
    text-align: center;
}


#moods {
    font-family: 'Barlow Condensed';
    padding: 50px 0;
}

#moods .moods-body {
    padding: 40px 20px;
    background: rgba(0,0,0,.1);
    min-height: 300px;
}

#moods .moods-body .moods-item:first-child {
    padding-left: 100px;
}

#moods .moods-body .moods-item:last-child {
    padding-right: 100px;
}

#moods .moods-title {
    font-family: 'Crimson';

}

.moods-list {
    display: flex;
    flex-direction: row;
    text-align: center;
    margin-top: 30px;
}
  
.moods-item {
    flex: 40%;
}

.moods-item:nth-child(2) {
    flex: 20%;
}

.moods-item img {
    width: 4em;
}


#blog {
    font-family: 'Barlow Condensed';
    padding: 50px 350px;
}

#blog .blog-body{
    overflow: auto;
}

#blog .blog-body .blog-image{
    float: left;
    width: 30%;
}

#blog .blog-body .blog-text{
    float: right;
    width: 70%;
    padding-left: 70px;
}

#blog .blog-body .blog-text h1{
    font-family: 'Crimson';
    padding-bottom: 15px;
    line-height :1;

}

#blog .blog-body .blog-text h2{
    text-transform: uppercase;
    padding-bottom: 5px;
}

#blog .blog-body .blog-text h3{
    font-size: 16px;
    line-height: 1.3;
    text-align: justify;
    padding-bottom: 20px;
    color: rgba(0,0,0,.7);
}

#blog .blog-body .blog-text p{
    padding-bottom: 20px;

}

#blog .blog-body .blog-text a, #blog .blog-body .blog-text i{
    text-transform: uppercase;
    color: rgba(0,0,0,.8);
}



#contact {
    font-family: 'Barlow Condensed';
    padding: 30px 50px;
    margin-top: 50px;
    background: rgb(46,46,46);
}

#contact .contact-body{
    overflow: auto;
    color: rgba(255,255,255,.7);
}

#contact .contact-body .contact-image{
    float: left;
    width: 30%;
    padding-right: 80px;
    text-align: right;
}

#contact .contact-body .contact-text{
    float: left;
    width: 35%;
    padding: 0 40px;
}

#contact .contact-body .contact-news{
    float: right;
    width: 35%;
    padding: 0 30px;
}

#contact .contact-body .contact-image img{
    width: 5em;
}

#contact .contact-body p{
    letter-spacing: 1px;
}

#contact .contact-body .contact-news p{
    letter-spacing: 1px;
    text-transform: uppercase;
}


#contact .contact-body .contact-news .news-form input{
    width: 60%;
    padding: 10px 12px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid rgba(255,255,255,.3);
    box-sizing: border-box;
    background: transparent;
}


#contact .contact-body .contact-news .news-form .btn-style{
    letter-spacing: 1px;
    padding: 7px 20px;
}



@media screen and (max-width: 1400px) {
    #blog {
        padding: 50px 200px;
    }
}

@media screen and (max-width: 1200px) {
    #blog {
        padding: 50px 150px;
    }

    #contact .contact-body .contact-image{
        width: 100%;
        text-align: center;
        padding: 5px 30px;
        
    }
    
    #contact .contact-body .contact-text{
        width: 100%;
        padding: 15px 30px;
        
        text-align: center;

    }
    
    #contact .contact-body .contact-news{
        width: 100%;
        padding: 5px 30px;
        text-align: center;

    }
}

@media screen and (max-width: 1000px) {

    nav {
        padding: 0 2.5em;
    }

    section h1 {
        font-size: 2.3em;
    }

    #banner .banner-body .banner-title h1 {
        font-size: 3.5em;
    }

    .column {
        max-width: 50%;
        flex: 50%;
        -ms-flex: 50%;
    }

    .moods-list {
        flex-direction: column;
    }

    #moods .moods-body .moods-item:first-child {
        padding-left: 0;
    }
    
    #moods .moods-body .moods-item:last-child {
        padding-right: 0;
    }

    #moods .moods-body .moods-item {
        padding: 15px 0;
    }

    #moods .moods-body .moods-item p{
        padding-top: 10px;
    }

    #blog {
        padding: 50px 100px;
    }
}

@media screen and (max-width: 800px) {
    #blog .blog-body .blog-image{
        float: left;
        width: 100%;
        text-align: center;
    }
    
    #blog .blog-body .blog-text{
        float: right;
        width: 100%;
        padding: 20px 0;
    }

}

@media screen and (max-width: 600px) {
    
    section h1 {
        font-size: 1.8em;
    }

    #banner .banner-body .banner-title h1 {
        font-size: 3em;
    }
    .column {
        max-width: 100%;
        flex: 100%;
        -ms-flex: 100%;
    }

    #blog {
        padding: 50px;
    }

    
}

@media screen and (min-width: 1000px) {
    .nav-item {
        display: block;
        width: auto;
    }
    .toggle-bars {
        display: none;
    }
    .logo {
        order: 0;
    }
}

@media screen and (min-width: 468px) {
    
    .nav-items {
        justify-content: center;
    }

    .logo {
        flex: 1;
    }
    
    
}


