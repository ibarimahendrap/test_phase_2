const header 		= document.querySelector('header nav');
const navToggle 	= document.querySelector('nav .nav__toggle');
const navLink 	    = document.querySelectorAll('nav .nav__link');

document.addEventListener('scroll', () => {
    // console.log(123);
	var scroll_position = window.scrollY;
    console.log(document.documentElement.scrollHeight - document.documentElement.clientHeight);
	if (scroll_position > 300) {
		header.style.backgroundColor = '#734d26';
	} else {
		header.style.backgroundColor = 'transparent';
	}
});

navToggle.addEventListener('click', () => {
    for (let i = 0; i < navLink.length; i++) {
        const elm       = navLink[i];
        elm.classList.toggle("active");
        
        let navActive   = elm.classList.contains("active");
        if(navActive){
            header.style.backgroundColor = '#734d26';
        } else {
            var scroll_position = window.scrollY;
            if (scroll_position > 300) {
                header.style.backgroundColor = '#734d26';
            } else {
                header.style.backgroundColor = 'transparent';
            }
        }
    }

});
